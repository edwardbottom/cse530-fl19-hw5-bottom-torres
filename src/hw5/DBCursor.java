package hw5;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class DBCursor implements Iterator<JsonElement>{
	
	public Iterator<JsonElement> iterator;
	
	/**
	 * 
	 * @param collection of objects
	 * @param query the parameters to be searched for
	 * @param fields the operations to be applied to the query
	 */
	public DBCursor(DBCollection collection, JsonObject query, JsonObject fields) {
		//list to store the values
		ArrayList<JsonElement> a = new ArrayList<>();
		
		//for all objects in the collection
		for(int i = 0; i < collection.count(); i++) {
			String match = query.toString().replaceAll("\\{", "").replaceAll("\\}", "");
			
			//if the collection has the field and value add it
			if(collection.contents.get(i).toString().contains(match)) {
				a.add(collection.contents.get(i));
			}
		}
		
		//variables for all the operators
		String field = "";
		String operation = "";
		String value = "";
		
		// parse for all the operations
		for(String j : fields.keySet()) {
			JsonObject c = (JsonObject) (fields.get(j));
			for(String i : c.keySet()) {
				field = i;
				JsonObject b = (JsonObject) c.get(i);
				for(String k : b.keySet()) {
					operation = k;
					value = b.get(k).toString().replace("\"", "");
				}
			}
		}
		
		//apply the needed operators
		a = applyOperator(a, field, operation, value);
		
		//set the iterator
		this.iterator = a.iterator();
	}
	
	/**
	 * 
	 * @param collection the collection to be queryed
	 * @param query the query to be applied
	 */
	public DBCursor(DBCollection collection, JsonObject query) {
		//list to store values
		ArrayList<JsonElement> a = new ArrayList<>();
		//store all values that match the query
		for(int i = 0; i < collection.count(); i++) {
			String match = query.toString().replaceAll("\\{", "").replaceAll("\\}", "");
			if(collection.contents.get(i).toString().contains(match)) {
				a.add(collection.contents.get(i));
			}
		}
		//update the iterator
		this.iterator = a.iterator();
	}
	
	/**
	 * 
	 * @param collection the collection to be returned 
	 */
	public DBCursor(DBCollection collection){
		this.iterator = collection.contents.iterator();
	}
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all objects containing the given field and value
	 */
	private ArrayList<JsonElement> equalsOperator(ArrayList<JsonElement> a, String field, String value) {
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonObject()) {
				//store all values equal to the given field
				if(j.getAsJsonObject().keySet().contains(field) && j.getAsJsonObject().get(field).toString().contains(value)) {
					newVals.add(j.getAsJsonObject());
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all objects with a field greater than the given value
	 */
	private ArrayList<JsonElement> greaterThanOperator(ArrayList<JsonElement> a, String field, String value){
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonObject()) {
				//add a value if it is greater than the given value
				if(j.getAsJsonObject().keySet().contains(field) && j.getAsJsonObject().get(field).toString().replace("\"", "").compareTo(value) > 0) {
					newVals.add(j.getAsJsonObject());
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all objects greater than or equal to the value
	 */
	private ArrayList<JsonElement> greaterThanOrEqualOperator(ArrayList<JsonElement> a, String field, String value){
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonObject()) {
				//add a value if it is greater than or equal to the value
				if(j.getAsJsonObject().keySet().contains(field) && j.getAsJsonObject().get(field).toString().replace("\"", "").compareTo(value) >= 0) {
					newVals.add(j.getAsJsonObject());
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all objects less than the given value
	 */
	private ArrayList<JsonElement> lessThanOperator(ArrayList<JsonElement> a, String field, String value){
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonObject()) {
				//add the given value if it contains a value low enough
				if(j.getAsJsonObject().keySet().contains(field) && j.getAsJsonObject().get(field).toString().replace("\"", "").compareTo(value) < 0) {
					newVals.add(j.getAsJsonObject());
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all objects where the given value is low enough
	 */
	private ArrayList<JsonElement> lessThanOrEqualOperator(ArrayList<JsonElement> a, String field, String value){
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonObject()) {
				//add the given value if it contains a value low enough
				if(j.getAsJsonObject().keySet().contains(field) && j.getAsJsonObject().get(field).toString().replace("\"", "").compareTo(value) <= 0) {
					newVals.add(j.getAsJsonObject());
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all objects not containing the given field and value
	 */
	private ArrayList<JsonElement> notEqualOperator(ArrayList<JsonElement> a, String field, String value) {
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonObject()) {
				//add the object if it does not contain the given value in the field
				if(j.getAsJsonObject().keySet().contains(field) && !j.getAsJsonObject().get(field).toString().contains(value)) {
					newVals.add(j.getAsJsonObject());
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all arrays containing the given field and value
	 */
	private ArrayList<JsonElement> inOperator(ArrayList<JsonElement> a, String field, String value) {
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonArray()) {
				for(int i = 0; i < j.getAsJsonArray().size(); i++) {
					JsonObject object = (JsonObject) j.getAsJsonArray().get(i);
					String match = "\"" + field + "\"" + ":" + "\"" + value + "\"";
					//add the array if it contains the given value
					if(object.toString().contains(match)) {
						newVals.add(j);
						break;
					}
				}
			}
		}
		return newVals;
	}
	
	/**
	 * 
	 * @param a the current list of objects
	 * @param field the field to project on
	 * @param value the value for the projection
	 * @return all arrays not containing the given field and value
	 */
	private ArrayList<JsonElement> ninOperator(ArrayList<JsonElement> a, String field, String value) {
		value.replace("\"", "");
		ArrayList<JsonElement> newVals = new ArrayList<>();
		for(JsonElement j: a) {
			if(j.isJsonArray()) {
				for(int i = 0; i < j.getAsJsonArray().size(); i++) {
					JsonObject object = (JsonObject) j.getAsJsonArray().get(i);
					String match = "\"" + field + "\"" + ":" + "\"" + value + "\"";
					//if the object contains the match, return
					if(object.toString().contains(match)) {
						break;
					}
					//add the array if it does not contain the given value
					if(i == j.getAsJsonArray().size() - 1) {
						newVals.add(j);
					}
				}
			}
		}
		return newVals;
	}
	/**
	 * 
	 * @param a the list of objects
	 * @param field the key value in the object
	 * @param operation the operation to be performed
	 * @param value the value for the field
	 * @return a list with the given projection
	 */
	private ArrayList<JsonElement> applyOperator(ArrayList<JsonElement> a, String field, String operation, String value) {
		if(operation.equals("$eq")) {
			a = equalsOperator(a, field, value);
		}else if(operation.equals("$gt")) {
			a = greaterThanOperator(a, field, value);
		}else if(operation.equals("$gte")){
			a = greaterThanOrEqualOperator(a, field, value);
		}else if(operation.equals("$in")) {
			a = inOperator(a,field,value);
		}else if(operation.equals("$lt")) {
			a = lessThanOperator(a, field, value);
		}else if(operation.equals("$lte")) {
			a = lessThanOrEqualOperator(a, field, value);
		}else if(operation.equals("$ne")) {
			a = notEqualOperator(a, field, value);
		}else if(operation.equals("$nin")) {
			a = ninOperator(a, field, value);
		}
		return a;
	}
	
	/**
	 * Returns true if there are more documents to be seen
	 */
	public boolean hasNext() {
		return iterator.hasNext();
	}

	/**
	 * Returns the next document
	 */
	public JsonElement next() {
		return (JsonElement) iterator.next();
	}
	
	/**
	 * Returns the total number of documents
	 */
	public long count() {
		int counter = 0;
		Iterator<JsonElement> copy = iterator;
		//increment for every object
		while (copy.hasNext()) {
		    counter++;
		    copy.next();
		}
		return counter;
	}

}
