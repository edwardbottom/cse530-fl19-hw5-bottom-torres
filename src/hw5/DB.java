package hw5;

import java.io.File;
import java.io.IOException;

public class DB {
	
	//the name of the db
	private String name;
	/**
	 * Creates a database object with the given name.
	 * The name of the database will be used to locate
	 * where the collections for that database are stored.
	 * For example if my database is called "library",
	 * I would expect all collections for that database to
	 * be in a directory called "library".
	 * 
	 * If the given database does not exist, it should be
	 * created.
	 */
	
	public DB(String name){
		this.name = name;
		String path = "testfiles/" + name;
		File f = new File(path);
		if (!f.exists()){
	        f.mkdir();
	    }
	}
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * Retrieves the collection with the given name
	 * from this database. The collection should be in
	 * a single file in the directory for this database.
	 * 
	 * Note that it is not necessary to read any data from
	 * disk at this time. Those methods are in DBCollection.
	 * @throws Exception 
	 */
	public DBCollection getCollection(String name){
		String path = "testfiles/" + name + ".json";
		return new DBCollection(this, name);
	}
	
	/**
	 * Drops this database and all collections that it contains
	 */
	public void dropDatabase() {
		//create the path to the db folder
		String path = "testfiles/" + name;
		File f = new File(path);
		
		//https://stackoverflow.com/questions/20281835/how-to-delete-a-folder-with-files-using-java
		// remove all files within the directory
		String[]entries = f.list();
		for(String s: entries){
		    File currentFile = new File(f.getPath(),s);
		    currentFile.delete();
		}
		
		//delete the directory itself
		f.delete();
		
	}
	
	
}
