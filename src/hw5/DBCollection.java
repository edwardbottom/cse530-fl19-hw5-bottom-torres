package hw5;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

//import java.util.List;

import com.google.gson.JsonObject;

public class DBCollection {

	public DB database;
	public String name;
	public ArrayList<JsonElement> contents;
	
	/**
	 * Constructs a collection for the given database
	 * with the given name. If that collection doesn't exist
	 * it will be created.
	 */
	public DBCollection(DB database, String name) {
		// intialize variables
		this.database = database;
		this.name = name;
		this.contents = new ArrayList<>();
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		
		//create the collection file if it does not exist
		File f = new File(path);
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		//read in the collection
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		contents.add(Document.parse(t));
	        	}
	        	
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }
	
	   
	}
	
	/**
	 * reloads the collection from the file
	 */
	private void refreshContents() {
		//clear the old collection
		contents.clear();
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		
		//read in the previous collection
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        
	        //read in all objects
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		JsonElement j = (JsonElement) Document.parse(t);
	        		contents.add(j);
	        	}
	        }
	    }
	        
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
	}
	
	/**
	 * Returns a cursor for all of the documents in
	 * this collection.
	 * @throws Exception 
	 */
	public DBCursor find(){
		refreshContents();
		return new DBCursor(this);
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @return
	 */
	public DBCursor find(JsonObject query) {
		refreshContents();
		return new DBCursor(this,query);
	}
	
	/**
	 * Finds documents that match the given query parameters.
	 * 
	 * @param query relational select
	 * @param projection relational project
	 * @return
	 */
	//the projection is the fields, query is relational select and project is relational project
	public DBCursor find(JsonObject query, JsonObject projection) {
		refreshContents();
		return new DBCursor(this,query,projection);
	}
	
	/**
	 * Inserts documents into the collection
	 * Must create and set a proper id before insertion
	 * When this method is completed, the documents
	 * should be permanently stored on disk.
	 * @param documents
	 */
	public void insert(JsonElement... documents) {
		//open the collection
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		File f = new File(path);
		try {
			if(!f.exists()) {
				//add the new collection list to the file
				f.createNewFile();
			}
			
			 // Create a new FileWriter object
            FileWriter fileWriter = new FileWriter(path);
            for(JsonElement j : documents) {
            	// Writing the jsonObject into sample.json
            	j = Document.addId(j);
                fileWriter.write(Document.toJsonString(j) + "|");
                ++Document.totalCountId;
                //contents.add(j);
            }
            fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	/**
	 * Locates one or more documents and replaces them
	 * with the update document.
	 * @param query relational select for documents to be updated
	 * @param update the document to be used for the update
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void update(JsonObject query, JsonObject update, boolean multi) {
		//query all values and store them
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		
		//clear the file
		File f = new File(path);
		if(!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		//list to store the collection
		ArrayList<JsonElement> objects = new ArrayList<>();
		
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        //store all values from the file
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		objects.add(Document.parse(t));
	        	}
	        	
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }
		
	    //string to check for values
	    String match = query.toString().replaceAll("\\{", "").replaceAll("\\}", "");
	    
		//apply the update
	    for(int i = 0; i < objects.size(); i++) {
	    	if(objects.get(i).toString().contains(match)) {
	    		objects.remove(i);
	    		objects.add(i,update);
	    		if(!multi) {
	    			break;
	    		}
	    	}
	    }
		
		//clear the file        
        if(f.delete()){
            try {
				f.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		
		//write to the file
        try {
			 // Create a new FileWriter object
            FileWriter fileWriter = new FileWriter(path);
            for(JsonElement j : objects) {
            	// Writing the jsonObject into sample.json
                fileWriter.write(Document.toJsonString(j) + "|");
            }
            fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	
	}
	
	/**
	 * Removes one or more documents that match the given
	 * query parameters
	 * @param query relational select for documents to be removed
	 * @param multi true if all matching documents should be updated
	 * 				false if only the first matching document should be updated
	 */
	public void remove(JsonElement query, boolean multi) {
		
		//list to store values
		ArrayList<JsonElement> objects = new ArrayList<>();
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		
		//open the file path
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        
	        //read in all objects
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		JsonElement j = (JsonElement) Document.parse(t);
	        		objects.add(j);
	        	}
	        }
	        
	        //clear the contents of the file
	        File f = new File(path);
	        
	        if(f.delete()){
	            f.createNewFile();
	        }
	        
	        //store all objects that need to be removed
	        ArrayList<JsonElement> rem = new ArrayList<>();
	        String match = query.toString().replaceAll("\\{", "").replaceAll("\\}", "");
	        
	        //remove the needed documents
	        for(int i = 0; i < objects.size(); i++) {
	        	if(objects.get(i).toString().contains(match)) {
	        		rem.add(objects.get(i));
	        		if(!multi) {
		        		break;
		        	}
	        	}
	        	
	        }
	        
	        //remove all objects from the query
	        for(int i = 0; i < rem.size(); i++) {
	        	objects.remove(rem.get(i));
	        }
	        
	        //rewrite to the files
			try {
				if(!f.exists()) {
					//add the new collection list to the file
					f.createNewFile();
				}
				
				 // Create a new FileWriter object
	            FileWriter fileWriter = new FileWriter(path);
	            for(JsonElement j : objects) {
	            	// Writing the jsonObject into sample.json
	                fileWriter.write(Document.toJsonString(j) + "|");
	            }
	            fileWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
	}
	
	/**
	 * Returns the number of documents in this collection
	 */
	public long count() {
		//store the file path
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		long count = 0;
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        //for each object in the file
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	//increment the counter
	        	count += data.length;
	        	
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }
	    return count;
	}
	
	/**
	 * 
	 * @return the name of the collection
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the ith document in the collection.
	 * Documents are separated by a line that contains only a single tab (\t)
	 * Use the parse function from the document class to create the document object
	 */
	public JsonElement getDocument(int i) {
		//store the file and count
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		long count = 0;
		
		//read in the file
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	//store each object
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int j = 0; i < data.length; j++) {
	        		if(count == i) {
	        			return Document.parse(data[j]);
	        		}
	        		++count;
	        	}
	        	
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	/**
	 * Drops this collection, removing all of the documents it contains from the DB
	 */
	public void drop() {
		String path = "testfiles/" + database.getName() + "/" + name + ".json";
		File f = new File(path);
		//delete the file holding the collection
		f.delete();
		contents.clear();
	}
	
}
