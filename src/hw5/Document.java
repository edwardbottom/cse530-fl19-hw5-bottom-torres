package hw5;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import com.google.gson.*;

public class Document {
	public static int totalCountId = 0;
	
	/**
	 * Parses the given json string and returns a JsonObject
	 * This method should be used to convert text data from
	 * a file into an object that can be manipulated.
	 */
	public static JsonElement parse(String json) {
		//return as null
		if(json == "") {
			return null;
		}
		JsonElement obj = new Gson().fromJson(json, JsonElement.class);
		//return as an array
		if(obj.isJsonArray()) {
			JsonArray a = (JsonArray) obj;
			return a;
		}else if(obj.isJsonPrimitive()) {
			//return as primitive
			return (JsonPrimitive) obj;
		}else{
			//return as object
			JsonObject j = (JsonObject) obj;
			return (JsonObject) j;
		}
	}
	
	public static JsonElement addId(JsonElement obj) {
		if(obj.isJsonArray()) {
			JsonArray a = (JsonArray) obj;
			JsonArray updatedA = new JsonArray();
			for(int i = 0; i < a.size(); i++) {
				if(a.get(i).isJsonObject()) {
					JsonObject j = a.get(i).getAsJsonObject();
					if(!j.has("_id")) {
						j.addProperty("_id", createId());
					}
					updatedA.add(j);
				}else if(a.get(i).isJsonArray()) {
					JsonArray sub = new JsonArray();
					for(int j = 0; j < a.get(i).getAsJsonArray().size(); j++) {
						JsonObject js = a.get(i).getAsJsonArray().get(j).getAsJsonObject();
						if(!js.has("_id")) {
							js.addProperty("_id", createId());
						}
						sub.add(js);
					}
					updatedA.add(sub);
				}
			}
			return updatedA;
		}else if(obj.isJsonPrimitive()) {
			//return as primitive
			return (JsonPrimitive) obj;
		}else{
			//return as object
			JsonObject j = (JsonObject) obj;
			if(!j.has("_id")) {
				j.addProperty("_id", createId());
			}
			return (JsonObject) j;
		}
	}
	/**
	 * Takes the given object and converts it into a
	 * properly formatted json string. This method should
	 * be used to convert JsonObjects to strings
	 * when writing data to disk.
	 */
	public static String toJsonString(JsonObject json) {
		String s = new Gson().toJson(json);
		return s;
	}
	
	/**
	 * Takes the given element and converts it into a
	 * properly formatted json string. This method should
	 * be used to convert JsonObjects to strings
	 * when writing data to disk.
	 */
	public static String toJsonString(JsonElement json) {
		String s = new Gson().toJson(json);
		return s;
	}
	
	public static String createId() {
		//the id system is based on the link below
		//https://docs.mongodb.com/manual/reference/method/ObjectId/
		//get the time stamp
		int unixTime = (int)(System.currentTimeMillis() / 1000);
		//randomize 5 bytes
		byte[] b = new byte[5];
		new Random().nextBytes(b);
		//3 byte counter of all objects that have been inserted (included deleted as well)
		byte[] counter = ByteBuffer.allocate(5).putInt(totalCountId).array();
		String id = unixTime + "";
		
		//append the random bytes
		for(int i = 0; i < b.length; i++) {
			id += b[i];
		}
		
		//append the counter bytes
		for(int i = 0; i < counter.length; i++) {
			id+= counter[i];
		}
		return id;
		
	}
}
