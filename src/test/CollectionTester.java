package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;
import hw5.Document;

class CollectionTester {
	
	@Test 
	public void testConstructor() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		assertTrue(premade.contents.size() == 3);
	}
	
	@Test
	/** 
	 * Check every object has "breakfast" and contains "eggs" as desired (Every object has breakfast and eggs)
	 */
	public void testFindAll() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		DBCursor results = premade.find();
		Iterator<JsonElement> copy = results.iterator;
		copy.next();
		while (copy.hasNext()) {
		    JsonObject j = (JsonObject) copy.next();
		    assertTrue(j.get("breakfast").toString().contains("eggs"));
		}
	}
	
	@Test 
	public void testFindArray() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("array");
		DBCursor results = premade.find();
		Iterator<JsonElement> copy = results.iterator;
		JsonElement j = copy.next();
		if(j.isJsonArray()) {
			JsonArray a = j.getAsJsonArray();
			assertTrue(a.size() == 3);
		}else {
			assertFalse(true);
		}
		
	}
	
	@Test
	public void testFindOneObject() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("single");
		String json = "{ \"hello\":\"world\"}";
		JsonObject param = (JsonObject) Document.parse(json);
		DBCursor results = premade.find(param);
		assertTrue(((JsonObject) results.iterator.next()).get("hello").toString().contains("world"));
	}
	
	@Test 
	public void testFindMultipleObjects() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		String json = "{ \"breakfast\":\"eggs\"}";
		JsonObject param = (JsonObject) Document.parse(json);
		DBCursor results = premade.find(param); 
		assertTrue(results.count() == 3);
	}
	
	@Test 
	public void testFindEmbedded() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("embedded");
		String json = "{ \"a\":\"test\"}";
		JsonObject param = (JsonObject) Document.parse(json);
		DBCursor results = premade.find(param); 
		JsonObject j = (JsonObject) results.iterator.next();
		j = ((JsonObject) j.get("test"));
		assertTrue(j.get("a").toString().contains("test"));
	}
	
	@Test
	public void testInsertPrimitive() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		
		premade.insert(obj);
		
		assertTrue(premade.count() == 1);
		
		String path  = "testfiles/premade/new.json";
		
		//read in and check the file
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		JsonObject j = (JsonObject) Document.parse(t);
	        		assertTrue(j.get("a").toString().contains("test"));
	        	}
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
	    
	  //remove the db
	   premade.drop();
	}
	
	@Test
	public void testInsertArray() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "[{ \"a\":\"test\"}, { \"a\":\"test\"}, { \"a\":\"test\"}]";
		JsonArray obj = (JsonArray) Document.parse(json);
		
		premade.insert(obj);
		
		assertTrue(premade.count() == 1);
		
		String path  = "testfiles/premade/new.json";
		
		//read in and check the file
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[0].replace("|","");
	        		JsonArray j = (JsonArray) Document.parse(t);
	        		assertTrue(((JsonObject)j.get(0)).get("a").toString().contains("test"));
	        	}
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
	    
	  //remove the db
	  premade.drop();
	}
	
	@Test
	public void testInsertEmbedded() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		JsonObject obj2 = (JsonObject) Document.parse(json);
		obj.add("emb", obj2);
		premade.insert(obj);
		
		assertTrue(premade.count() == 1);
		
		String path  = "testfiles/premade/new.json";
		//read in and check the file
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[0].replace("|","");
	        		JsonObject j = (JsonObject) Document.parse(t);
	        		assertTrue(j.toString().contains("emb"));
	        	}
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
	    
	  //remove the db
	  premade.drop();
	}
	
	@Test
	public void testInsertMany() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		
		premade.insert(obj, obj, obj, obj);
		
		assertTrue(premade.count() == 4);
		
		//remove the db
		premade.drop();
	}
	
	@Test
	public void testGetCount() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		assertTrue(3 == premade.count());
	}
	
	@Test 
	public void testGetName() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		assertTrue(premade.name == "collection");
	}
	
	@Test 
	public void testDropCollection() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("temp");
		String path = "testfiles/premade/temp.json";
		File f = new File(path);
		assertTrue(f.exists());
		premade.drop();
		assertFalse(f.exists());
	}
	
	@Test
	public void testGetDocument() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		assertTrue(((JsonObject)premade.getDocument(1)).get("breakfast").toString().contains("eggs"));
	}
	
	@Test
	public void testRemoveOne() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		
		premade.insert(obj, obj, obj, obj);
		
		assertTrue(premade.count() == 4);
		
		premade.remove(obj, false);
		
		assertTrue(premade.count() == 3);
		//remove the db
		premade.drop();
	}
	
	@Test 
	public void testRemoveMany() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		
		premade.insert(obj, obj, obj, obj);
		
		assertTrue(premade.count() == 4);
		
		premade.remove(obj, true);
		
		assertTrue(premade.count() == 0);
		
		//remove the db
		premade.drop();
	}
	
	@Test 
	public void testRemoveArray() {
		DB db = new DB("premade");
		
		DBCollection premade = db.getCollection("new");
		String json = "[{ \"a\":\"test\"}, { \"a\":\"test\"}, { \"a\":\"test\"}]";
		JsonArray obj = (JsonArray) Document.parse(json);
		
		premade.insert(obj);
		
		assertTrue(premade.count() == 1);
		String remString = "{ \"a\":\"test\"}";
		JsonObject remObj = (JsonObject) Document.parse(remString);
		premade.remove(remObj, false);
		
		assertTrue(premade.count() == 0);
		
		//remove the db
		premade.drop();
	}
	
	@Test
	public void testRemoveEmbedded() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		JsonObject obj2 = (JsonObject) Document.parse(json);
		obj.add("emb", obj2);
		premade.insert(obj);
		
		assertTrue(premade.count() == 1);
		
		premade.remove(obj2, true);
		
		assertTrue(premade.count() == 0);
	  //remove the db
	  premade.drop();
	}
	
	@Test 
	public void testUpdateOne() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		
		premade.insert(obj, obj);
		
		assertTrue(premade.count() == 2);
		
		String newJson = "{ \"b\":\"test\"}";
		JsonObject newObj = (JsonObject) Document.parse(newJson);
		
		premade.update(obj, newObj, false);
		
		assertTrue(premade.count() == 2);
		
		String path  = "testfiles/premade/new.json";
		
		ArrayList<JsonObject> results = new ArrayList<>();
		
		//read in and check the file
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		JsonObject j = (JsonObject) Document.parse(t);
	        		results.add(j);
	        	}
	        }
	        	
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
		
	    assertTrue(results.size() == 2);
	    
	    for(int i = 0; i < results.size(); i++) {
	    	if(i == 0) {
	    		assertTrue(results.get(i).toString().contains("b"));
	    	}else if(i == 1) {
	    		assertTrue(results.get(i).toString().contains("a"));
	    	}
	    }
	    
		//remove the db
		premade.drop();
	}
	
	@Test
	public void testUpdateMany() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("new");
		String json = "{ \"a\":\"test\"}";
		JsonObject obj = (JsonObject) Document.parse(json);
		
		premade.insert(obj,obj);
		
		assertTrue(premade.count() == 2);
		
		String newJson = "{ \"b\":\"test\"}";
		JsonObject newObj = (JsonObject) Document.parse(newJson);
		
		premade.update(obj, newObj, true);
		
		assertTrue(premade.count() == 2);
		
		String path  = "testfiles/premade/new.json";
		
		//read in and check the file
		//https://howtodoinjava.com/java/io/java-read-file-to-string-examples/
	    try (BufferedReader br = new BufferedReader(new FileReader(path)))
	    {
	        String sCurrentLine;
	        StringBuilder contentBuilder = new StringBuilder();
	        while ((sCurrentLine = br.readLine()) != null) 
	        {	
	        	String s = sCurrentLine.toString();
	        	String[] data = s.split("\\|");
	        	for(int i = 0; i < data.length; i++) {
	        		String t = data[i].replace("|","");
	        		JsonObject j = (JsonObject) Document.parse(t);
	        		assertTrue(j.toString().contains("b"));
	        	}
	        	
	        }
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	        assertTrue(false);
	    }
		
		//remove the db
		premade.drop();
	}
	
	
	
	@Test
	public void testEq() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		String json = "{ \"breakfast\":\"eggs\"}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"go\":{ \"$eq\":\"breakfast\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 1);
	}
	
	@Test
	public void testGt() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("vals");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"val\":{ \"$gt\":\"2\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 2);
	}
	
	@Test
	public void testGte() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("vals");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"val\":{ \"$gte\":\"2\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 3);
	}
	
	@Test
	public void testIn() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("array");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"contents\":{ \"$in\":\"two\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 1);
	}
	
	@Test
	public void testLt() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("vals");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"val\":{ \"$lt\":\"3\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 2);
	}
	
	@Test
	public void testLte() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("vals");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"val\":{ \"$lte\":\"3\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 3);
	}
	
	@Test
	public void testNe() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("vals");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"val\":{ \"$ne\":\"1\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 3);
	}
	
	@Test 
	public void testNotIn() {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("vals");
		String json = "{}";
		JsonObject param = (JsonObject) Document.parse(json);
		
		String search = "{\"field\":{\"contents\":{ \"$nin\":\"two\"}}}";//<field>: { $eq: <value> }
		JsonObject sear = (JsonObject) Document.parse(search);
		
		
		DBCursor results = premade.find(param, sear); 
		assertTrue(results.count() == 0);
	}
	
}
