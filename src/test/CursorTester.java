package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

import hw5.DB;
import hw5.DBCollection;
import hw5.DBCursor;

class CursorTester {
	@Test 
	/**
	 * Finds everything in the collection that exists
	 */
	public void testFindAll() throws Exception {
		DB db = new DB("premade");
		DBCursor premade = db.getCollection("collection").find();
		assertTrue(premade.count() == 3);
		
	}
	
	@Test
	/**
	 * Checks 'hasNext' returns true if there are more documents to be seen
	 */
	public void testHasNext() throws Exception {
		DB db = new DB("premade");
		DBCursor premade = db.getCollection("collection").find();
		while(premade.hasNext()) {
			assertTrue(premade.next() != null);
		}
		assertFalse(premade.hasNext());
	}
	
	@Test
	/**
	 * Checks 'hasNext = False' for a collection with no documents 
	 */
	public void testNullCursor() throws Exception {
		DB db = new DB("new");
		DBCursor premade = db.getCollection("collection").find();
		assertFalse(premade.hasNext());
	}
	
	@Test
	/**
	 * Checks the number of documents in the collection is correct
	 */
	public void testCount() throws Exception {
		DB db = new DB("premade");
		DBCollection premade = db.getCollection("collection");
		assertTrue(premade.count() == 3);
	}
	
	
}
