package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.Test;

import hw5.DB;
import hw5.DBCollection;

class DBTester {
	@Test
	public void testCreateDB() {
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
	}
	
	@Test 
	public void testDropEmptyDB() {
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
		hw5.dropDatabase();
		assertFalse(new File("testfiles/hw5").exists());
	}
	
	@Test 
	public void testDropDB() {
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
		hw5.getCollection("a");
		hw5.getCollection("b");
		hw5.getCollection("c");
		hw5.dropDatabase();
		assertFalse(new File("testfiles/hw5").exists());
	}
	
	@Test 
	public void createCollection() {
		DB hw5 = new DB("hw5");
		assertTrue(new File("testfiles/hw5").exists());
		hw5.getCollection("a");
		File f = new File("testfiles/hw5/a.json");
		assertTrue(f.exists());
		hw5.dropDatabase();
		assertFalse(new File("testfiles/hw5").exists());
	}
	
	
	@Test
	public void accessExistingDirectory() {
		assertTrue(new File("testfiles/premade").exists());
		DB premade = new DB("testfiles/premade");
		assertTrue(new File("testfiles/premade").exists());
	}
	
	@Test 
	public void accessExistingCollection() {
		DB premade = new DB("premade");
		DBCollection col = premade.getCollection("collection");
		assertTrue(col.count() == 3);
	}
	
	
}
