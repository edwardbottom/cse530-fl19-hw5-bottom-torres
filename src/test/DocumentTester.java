package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

import hw5.Document;

class DocumentTester {
	@Test
	public void testParsePrimitive() {
		String json = "{ \"key\":\"value\"}";
		JsonObject results = (JsonObject) Document.parse(json);
		assertTrue(results.getAsJsonPrimitive("key").getAsString().equals("value"));		
	}
	
	@Test
	public void testPrimitiveString() {
		JsonObject obj = new JsonObject();
		obj.addProperty("key", "value");
		String s = Document.toJsonString(obj);
		assertTrue(s.equals(s.toString()));
	}
	
	@Test
	/**
	 *Test invalid Json (left out a bracket) 
	 */
	public void invalidJsonParse() {
		String s = "{ \"key\":\"value\"";
		try {
			Document.parse(s);
			assertFalse(true);
		}catch(Exception e) {
			assertTrue(true);
		}
	}
	
	@Test 
	public void embeddedtoString() {
		JsonObject obj = new JsonObject();
		JsonObject emb = new JsonObject();
		emb.addProperty("key", "value");
		obj.add("obj", emb);
		assertTrue(obj.toString().equals(Document.toJsonString(obj)));
	}
	
	@Test 
	public void embeddedParse() {
		JsonObject obj = new JsonObject();
		JsonObject emb = new JsonObject();
		emb.addProperty("key", "value");
		obj.add("obj", emb);
		String s = obj.toString();
		JsonObject newObj = (JsonObject) Document.parse(s);
		String stringify = newObj.getAsJsonObject("obj").getAsJsonPrimitive("key").toString();
		assertTrue(stringify.equals(emb.get("key").toString()));
	}
	
	@Test
	public void parseEmbeddedArray() {
		String s = "{\"array\":[{\"a\":\"a\"},{\"b\":\"b\"},{\"c\":\"c\"}]}";
		JsonObject arrObj = (JsonObject) Document.parse(s);
		JsonArray arr = arrObj.getAsJsonArray("array");
		assertTrue(arr.size() == 3);
	}
	
	@Test 
	public void testArrayOfObjects() {
		String s = "[{\"a\":\"a\"},{\"b\":\"b\"},{\"c\":\"c\"}]";
		JsonArray arr = (JsonArray) Document.parse(s);
		assertTrue(arr.size() == 3);
	}
	
	@Test 
	public void testArrayOfArrays() {
		String s = "[[{\"a\":\"a\"}],[{\"b\":\"b\"}],[{\"c\":\"c\"}]]";
		JsonArray arr = (JsonArray) Document.parse(s);
		assertTrue(arr.size() == 3);
	}
	
	@Test
	/**
	 * Test null, make null object and insure null is returned
	 */
	public void testNullable() {
		JsonNull n = (JsonNull) Document.parse("");
		assertTrue(n == null);
	}
	
	@Test 
	/**
	 * Test ToString method 
	 */
	public void arrayToString() {
		JsonObject arrObj = new JsonObject();
		JsonArray arr = new JsonArray();
		JsonObject emb = new JsonObject();
		emb.addProperty("key", "value");
		arr.add(emb);
		String s = Document.toJsonString(arrObj);
		assertTrue(s.equals(arrObj.toString()));
	}
	
	@Test 
	public void uniqueId() {
		assertFalse(Document.createId().equals(Document.createId()));
	}
	

}
